package com.navyvehicle.navyvehicle.retrofit;


import com.navyvehicle.navyvehicle.model.LoginData;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {


    @POST("/api/login")
    Call<LoginData> getAllLoginData(@Query("contact_no") String contact_no,
                                    @Query("password") String password);

//    @POST("/api/bcsalllevel")
//    Call<LevelClass> getAllLevelData(@Query("p_cat") String id);
//
//    @POST("/api/bcsquestions")
//    Call<Quiz> getAllQuestionAndAnswer(@Query("p_cat") String id);

}