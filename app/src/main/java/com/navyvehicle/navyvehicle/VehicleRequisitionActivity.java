package com.navyvehicle.navyvehicle;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;
import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.Callback.OnRequisitionCallback;
import com.navyvehicle.navyvehicle.adapter.RequisitionAdapter;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import retrofit2.Call;

public class VehicleRequisitionActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnLoginValueListener, OnRequisitionCallback, SwipeRefreshLayout.OnRefreshListener {

    private static final String CATEGORY_IMAGE_URL = AppController.
            BASE_URL + "/public/uploads/thumbnail/";
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private MySharedPreparence preparence;
    private RecyclerView recyclerView;
    private ArrayList<LoginData.Requisition> requisitionsList, requisitionsFilteredList;
    private LoginResponse loginResponse;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm a");
    private ImageView imageViewRequisition, imageViewOfficer;
    private TextView tvOfficeName, tvWelcome;
    private LoginData loginData;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isFromRefresh;
    private LinearLayout llWelcomePage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_requisition);
        prepareSwipeRefresh();


    }

    @Override
    protected void onStart() {
        super.onStart();

        setMaterialDesign();
        init();
        if (loginData == null) {
            loadData();
        } else {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView();
        }
    }

    private void init() {
        requisitionsList = new ArrayList<>();
        requisitionsFilteredList = new ArrayList<>();
        preparence = new MySharedPreparence(this);

        tvWelcome = findViewById(R.id.tvWelcome);
        tvWelcome.setText("Welcome, " + preparence.getName());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        loginResponse = new LoginResponse(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        imageViewRequisition = (ImageView) findViewById(R.id.imageViewAmenity);
        llWelcomePage = findViewById(R.id.llWelcomePage);


        imageViewRequisition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                startActivity(new Intent(VehicleRequisitionActivity.this,
                        AddRequisitionActivity.class));
            }
        });

    }

    private void prepareSwipeRefresh() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorScheme(
                R.color.black, R.color.speakers,
                R.color.semenears, R.color.venue);

    }


    @Override
    public void onRefresh() {
        // Start showing the refresh animation
        mSwipeRefreshLayout.setRefreshing(true);

        isFromRefresh = true;
        loadData();
    }


    private void prepareRecycleView() {

        if (requisitionsList != null)
            sortByDate();
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);

        if (requisitionsFilteredList != null && requisitionsFilteredList.size() == 0) {
            llWelcomePage.setVisibility(View.VISIBLE);
        } else {
            llWelcomePage.setVisibility(View.GONE);
        }


        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        RequisitionAdapter requisitionAdapter = new RequisitionAdapter(
                requisitionsFilteredList, this, loginData,
                this);
        recyclerView.setAdapter(requisitionAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
        if (loginData != null && loginData.getUsers() != null) {
            Picasso.with(this).load(CATEGORY_IMAGE_URL + loginData.getUsers().getPhoto())
                    .into(imageViewOfficer);
            tvOfficeName.setText(loginData.getUsers()
                    .getFirst_name() + " " + loginData.getUsers().getLast_name());

        }
    }

    private void filterList() {
        LoginData.Requisition requisition = null;
        boolean flag = true;
        int counter = 0;
        for (int i = 0; i < requisitionsList.size(); i++) {
            if (requisitionsList.get(i).getStatus() != null) {
                if (requisitionsList.get(i).getStatus().equals("Rejected")) {


                } else if (requisitionsList.get(i).getStatus().equals("Completed")) {
                    if (flag) {
                        requisition = requisitionsList.get(i);
                    }
                    flag = false;
                    //requisitionsFilteredList.add(requisitionsList.get(i));
                } else {
                    requisitionsFilteredList.add(requisitionsList.get(i));

                }
            }
        }
//        Toast.makeText(this, "requisitionsFilteredList "+requisitionsFilteredList
//                .size(), Toast.LENGTH_SHORT).show();
//        if (requisition != null) {
//            requisitionsFilteredList.add(requisition);
//        }
    }


    private void sortByDate() {
        Collections.sort(requisitionsList, new Comparator<LoginData.Requisition>() {
            @Override
            public int compare(LoginData.Requisition o1, LoginData.Requisition o2) {
                int compareResult = 0;
                try {
                    Date arg0Date = format.parse(o1.getStart_time());
                    Date arg1Date = format.parse(o2.getStart_time());
                    compareResult = arg1Date.compareTo(arg0Date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    compareResult = o2.getStart_time().compareTo(o1.getStart_time());
                }
                return compareResult;
            }
        });
        filterList();
    }


    @Override
    public void customeShortClick(LoginData.Requisition requisition, int position) {

    }

    @Override
    public void customeLongtClick(LoginData.Requisition requisition, int position) {

    }

    private void loadData() {
        requisitionsFilteredList = new ArrayList<>();
        requisitionsList = new ArrayList<>();
        Call<LoginData> call = apiInterface.getAllLoginData(preparence.getPhoneNumber(),
                preparence.getPassword()
        );
        call.enqueue(loginResponse);

        if (!isFromRefresh)
            progressDialog.show();
    }


    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        progressDialog.dismiss();
        //   AppController.getInstance().setLoginData(loginData);
        this.loginData = loginData;
        mSwipeRefreshLayout.setRefreshing(false);
        if (loginData != null) {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView();
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {
        mSwipeRefreshLayout.setRefreshing(false);
        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }


    private void setMaterialDesign() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dashboard");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        imageViewOfficer = header.findViewById
                (R.id.imageViewOfficer);
        tvOfficeName = header.findViewById(R.id.tvOfficerName);
        imageViewOfficer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(VehicleRequisitionActivity.this,
                        ProfileActivity.class));
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

//      else if (id == R.id.nav_Profile) {
//            startActivity(new Intent(VehicleRequisitionActivity.this,
//                    ProfileActivity.class));
//
//        }

        int id = item.getItemId();

        if (id == R.id.nav_my_trips) {
            startActivity(new Intent(VehicleRequisitionActivity.this,
                    MyTripActivity.class));
        } else if (id == R.id.nav_dashboard) {

            Intent intent = getIntent();
            finish();
            startActivity(intent);

        } else if (id == R.id.nav_amenity_history) {
            startActivity(new Intent(VehicleRequisitionActivity.this,
                    AmenityHistoryActivity.class));
        } else if (id == R.id.nav_requisition) {
            finish();
            startActivity(new Intent(VehicleRequisitionActivity.this,
                    AddRequisitionActivity.class));
        } else if (id == R.id.nav_Logout) {
            if (loginData != null && loginData.getUsers() != null) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic
                        (loginData.getUsers().getContact_no());
            }
            preparence.setLoginStatus(false);
            preparence.setPassword("");
            preparence.setPhoneNumber("");
            finish();
            startActivity(new Intent(VehicleRequisitionActivity.this,
                    LoginActivity.class));
        } else if (id == R.id.nav_contact_us) {
            startActivity(new Intent(VehicleRequisitionActivity.this,
                    ContactUsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
