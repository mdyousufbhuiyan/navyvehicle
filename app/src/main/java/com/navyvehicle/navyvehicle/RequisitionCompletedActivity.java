package com.navyvehicle.navyvehicle;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

public class RequisitionCompletedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requisition_completed);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Requisition");
    }

    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(RequisitionCompletedActivity.this,
                AddRequisitionActivity.class);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                Intent intent = new Intent(RequisitionCompletedActivity.this,
                        AddRequisitionActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
