package com.navyvehicle.navyvehicle;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by issl on 2/15/2018.
 */

public class MySharedPreparence {
    private Context context;
    SharedPreferences preferences;
    private static final String SHARED_PREFERENCE_NAME = "mysharedprefarence";
    private static final String LOGIN_STATUS = "login";
    private static final String PHONE_NUMBER = "phonenumber";
    private static final String PASSWORD = "password";
    private static final String NAME = "name";
    private static final String POOL = "pool";


    public MySharedPreparence(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        this.context = context;
    }

    public void setLoginStatus(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_STATUS, value);

        editor.commit();
    }

    public void setName(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NAME, value);
        editor.commit();
    }

    public boolean getLoginStaus() {
        return preferences.getBoolean(LOGIN_STATUS, false);
    }

    public void setPhoneNumber(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PHONE_NUMBER, value);

        editor.commit();
    }

    public String getPhoneNumber() {
        return preferences.getString(PHONE_NUMBER, "");
    }


    public String getName() {
        return preferences.getString(NAME, "");
    }

    public void setPassword(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PASSWORD, value);

        editor.commit();
    }

    public String getPassword() {
        return preferences.getString(PASSWORD, "");
    }

    public void setPool(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(POOL, value);

        editor.commit();
    }

    public String getPool() {
        return preferences.getString(POOL, "");
    }
}
