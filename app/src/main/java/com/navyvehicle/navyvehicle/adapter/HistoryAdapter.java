package com.navyvehicle.navyvehicle.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.navyvehicle.navyvehicle.AppController;
import com.navyvehicle.navyvehicle.Callback.OnRequisitionCallback;
import com.navyvehicle.navyvehicle.R;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryAdapterViewHolder> {
    public static final String CATEGORY_IMAGE_URL = AppController.BASE_URL + "/public/uploads/thumbnail/";

    private List<LoginData.Requisition> requisitionList;
    Context context;
    OnRequisitionCallback onRequisitionCallback;

    HistoryAdapterViewHolder holder;
    private LoginData loginData;
    private String status, input;

    public HistoryAdapter(List<LoginData.Requisition> requisitionList,
                          LoginData loginData, Context context,
                          OnRequisitionCallback onRequisitionCallback, String status) {
        this.requisitionList = requisitionList;
        this.context = context;
        this.onRequisitionCallback = onRequisitionCallback;
        this.loginData = loginData;
        this.status = status;
    }

    @Override
    public HistoryAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_amenity_report_layout, parent, false);
        HistoryAdapterViewHolder cvh = new HistoryAdapterViewHolder(view);

        return cvh;
    }

    @Override
    public void onBindViewHolder(HistoryAdapterViewHolder holder1, final int position) {
        this.holder = holder1;
        LoginData.Requisition requisition = requisitionList.get(position);
        if (position == 0 && !status.equals("current") && !status.equals("completed")) {
            holder1.llAmenity.setVisibility(View.VISIBLE);
            if (loginData != null) {
                holder.tvTotalAmenity.setText(loginData.getTotal_amenity() + " \n BDT ");
                holder.tvPaidAmenity.setText(loginData.getPaid_amenity() + " \n BDT ");
                holder.tvDueAmenity.setText(loginData.getDue_amenity() + " \n BDT ");
            }
            holder1.llAmenity.setVisibility(View.GONE);
        } else {
            holder1.llAmenity.setVisibility(View.GONE);
        }


        holder.tvDate.setText(getDate(requisition.getStart_time()));
        if (requisition.getStatus() != null &&
                requisition.getStatus().equals("Completed")) {
            holder.tvTime.setText("Duration : " + (requisition.getHour()));
        } else {
            holder.tvTime.setText("Est. " + getEstimatedHour(requisition.getStart_time(),
                    requisition.getEnd_time()) + "h");
        }

        if (requisition.getAmenity_check() == 0)
            holder.tvAmenity.setText("Est. ৳ " + requisition.getAmenity());
        else
            holder.tvAmenity.setText(requisition.getAmenity() + " ৳ ");

        if (requisition.getVehicle() != null)
            holder.tvVehicaleCategory.setText(requisition.getVehicle());
        else {
            holder.tvVehicaleCategory.setText(requisition.getCategory());
        }
        if ((position + 1) % 2 == 0) {
            holder1.llRequisition.setBackgroundColor(
                    Color.parseColor("#D7D7D7"));

        } else {
            holder1.llRequisition.setBackgroundColor(
                    Color.parseColor("#BAE3F4"));
        }


        //.............driver details...........................
        if (requisition.isExpanded()) {
            holder1.llExpandView.setVisibility(View.VISIBLE);
        } else {
            holder1.llExpandView.setVisibility(View.GONE);
        }


        if (requisition.getDriver_name() != null)
            holder1.tvDriverNameDetails.setText("You ride with " + requisition.getDriver_name());
        if (requisition.getHour() != null)
            holder1.tvTotalOur.setText("Duration : " + requisition.getHour());
        if (requisition.getAmenity() != null)
            holder.tvAmenityDetails.setText("Amenity : " + requisition.getAmenity()
                    + " BDT");
        else {
            holder.tvVehicaleCategory.setText(requisition.getCategory());
        }

        input = requisition.getDriver_photo();
        if (input != null)
            if (!Patterns.WEB_URL.matcher(input).matches()) {
                try {
                    input = URLEncoder.encode(input, "utf-8");
//                Picasso.with(context).load(CATEGORY_IMAGE_URL + input.toString()).into(holder.imageView);


                    Picasso.with(context).load(CATEGORY_IMAGE_URL + input.toString()).networkPolicy(NetworkPolicy.OFFLINE)
                            .into(holder.imageViewDriver, new Callback() {
                                @Override
                                public void onSuccess() {
                                    //Toast.makeText(getApplicationContext(), "from offline ", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onError() {
                                    Picasso.with(context).load(CATEGORY_IMAGE_URL + input.toString()).into(holder.imageViewDriver);
                                    // Toast.makeText(getApplicationContext(), "from online ", Toast.LENGTH_LONG).show();
                                }
                            });


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Picasso.with(context).load(CATEGORY_IMAGE_URL + input.toString())
                        .into(holder.imageViewDriver);
            }


    }

    @Override
    public int getItemCount() {
        return requisitionList.size();
    }

    public class HistoryAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        TextView tvTotalAmenity, tvPaidAmenity, tvDueAmenity, tvDate, tvAmenity, tvTime,
                tvVehicaleCategory, tvTotalOur, tvAmenityDetails, tvDriverNameDetails;
        LinearLayout llAmenity, llExpandView, llRequisition;
        ImageView imageViewDriver;

        public HistoryAdapterViewHolder(View view) {
            super(view);
            tvTotalAmenity = view.findViewById(R.id.tvTotalAmenity);
            tvDueAmenity = view.findViewById(R.id.tvDueAmenity);
            tvPaidAmenity = view.findViewById(R.id.tvPaidAmenity);
            tvDate = view.findViewById(R.id.tvDate);
            tvTime = view.findViewById(R.id.tvTime);
            tvAmenity = view.findViewById(R.id.tvAmenity);
            tvVehicaleCategory = view.findViewById(R.id.tvVehicaleCategory);
            tvTotalOur = view.findViewById(R.id.tvTotalOur);
            tvAmenityDetails = view.findViewById(R.id.tvAmenityDetails);
            tvDriverNameDetails = view.findViewById(R.id.tvDriverNameDetails);
            imageViewDriver = view.findViewById(R.id.imageViewDriver);

            llAmenity = view.findViewById(R.id.llAmenity);
            //  llHeader = view.findViewById(R.id.llHeader);
            llRequisition = view.findViewById(R.id.llRequisition);
            llExpandView = view.findViewById(R.id.llExpandView);
            llRequisition.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            boolean flag = requisitionList.get(position).isExpanded();
            if (status.equals("trips")) {
                if (flag) {
                    for (LoginData.Requisition r : requisitionList) {
                        r.setExpanded(false);
                    }
                } else {
                    for (LoginData.Requisition r : requisitionList) {
                        r.setExpanded(false);
                    }
                    requisitionList.get(position).setExpanded(true);
                }
                notifyDataSetChanged();
            }
            onRequisitionCallback.customeShortClick(requisitionList.get(position), position);

        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();
            onRequisitionCallback.customeShortClick(requisitionList.get(position), position);
            return true;
        }
    }

    private String getDate(String date) {

        // String date = "Mar 10, 2016 6:30:00 PM";
        // SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd/MM/yy HH:mm");
        if (newDate != null)
            date = spf.format(newDate);
        return date;
    }


    private int getEstimatedHour(String startTime, String endTime) {

        try {
//            String date1 = "07/15/2016";
//            String time1 = "11:00 AM";
//            String date2 = "07/17/2016";
//            String time2 = "12:15 AM";

            //  String format = "MM/dd/yyyy hh:mm";
            //HH converts hour in 24 hours format (0-23), day calculation
            String format = "yyyy-MM-dd HH:mm:ss";

            //SimpleDateFormat sdf = new SimpleDateFormat(format);

            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);

            Date dateObj1 = sdf.parse(endTime);
            Date dateObj2 = sdf.parse(startTime);
            System.out.println(dateObj1);
            System.out.println(dateObj2 + "\n");

            DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");

            // getTime() returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this Date object
            long diff = dateObj1.getTime() - dateObj2.getTime();

//            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
//            System.out.println("difference between days: " + diffDays);

            int diffhours = (int) (diff / (60 * 60 * 1000));
            double remainHour = (diff % (60 * 60 * 1000));

            if (remainHour > 0) {
                diffhours += 1;
            }

            return diffhours;


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Date", "" + e.getMessage());
        }

        return 0;
    }


}