package com.navyvehicle.navyvehicle;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;

public class ProfileActivity extends AppCompatActivity implements OnLoginValueListener {

    String[] PERMISSIONS = {"android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.READ_EXTERNAL_STORAGE"};

    private String UPLOAD_URL = "http://navyvehicle.isslsolutions.com/api/update";
    private static final String CATEGORY_IMAGE_URL = AppController.BASE_URL + "/public/uploads/thumbnail/";
    private static final int REQUEST_CODE = 12;
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private LoginResponse loginResponse;
    private ImageView imageViewOfficer;
    private TextView tvOfficerName, tvNavalId, tvEmail, tvPassword;
    private Spinner spAssignPool;
    private EditText etPhone;
    private LoginData.Users users;
    private ArrayList<LoginData.Pool> poolsList;
    private MySharedPreparence preparence;
    private LoginData loginData;
    private File imageFile;
    private Button btnUpdate;
    private ArrayList<String> poolNameList;
    private String strPool;
    private int selectionPosition = 0;
    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");


        init();
        checkPermission();

    }

    private void init() {
        handler = new Handler();
        poolNameList = new ArrayList<>();
        poolNameList = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        loginResponse = new LoginResponse((OnLoginValueListener) this);
        preparence = new MySharedPreparence(this);
        spAssignPool = findViewById(R.id.spAssignPool);
        imageViewOfficer = findViewById(R.id.imageViewOfficer);
        tvOfficerName = findViewById(R.id.tvOfficerName);
        tvNavalId = findViewById(R.id.tvNavalId);
        etPhone = findViewById(R.id.etPhone);
        btnUpdate = findViewById(R.id.btnUpdate);
        tvEmail = findViewById(R.id.tvEmail);


        etPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnUpdate.setVisibility(View.VISIBLE);
            }
        });


        if (loginData == null) {
            loadData();
        } else {
            setData();
        }


    }

    private void setData() {
        users = loginData.getUsers();
        tvOfficerName.setText(users.getFirst_name() + "\n" + users.getLast_name());
        tvNavalId.setText("P. Number: " + users.getNaval_id());
        tvEmail.setText(users.getEmail());
        etPhone.setText(users.getContact_no());


        Picasso.with(this).load(CATEGORY_IMAGE_URL + users.getPhoto()).into(imageViewOfficer);

        setPoolNameList();
        imageViewOfficer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });


    }

    private void setPoolNameList() {
        int i = 0;
        for (LoginData.Pool pn : poolsList) {
            poolNameList.add(pn.getName());
            if (pn.getId() == Integer.valueOf(users.getAssigned_pool())) {
                selectionPosition = i;
            }
            i++;
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.custome_spinner_layout
                , R.id.tvPoolName, poolNameList);
        spAssignPool.setAdapter(arrayAdapter);
        spAssignPool.setSelection(selectionPosition);
        spAssignPool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != selectionPosition) {
                    strPool = String.valueOf(poolsList.get(position).getId());
                    btnUpdate.setVisibility(View.VISIBLE);
                } else
                    strPool = String.valueOf(selectionPosition);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void btnUpdate(View view) {
        makeHttpRequest();

    }


    private void makeHttpRequest() {
        final String contantNo = etPhone.getText().toString();


        progressDialog.show();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {


                OkHttpClient client = new OkHttpClient();
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM);


                RequestBody requestBody1;

//                RequestBody requestBody1 = new MultipartBody.Builder()
//                        .setType(MultipartBody.FORM)


                builder.addFormDataPart("contact_no", contantNo)

                        .addFormDataPart("officer_id", String.valueOf(users.getId()))
                        .addFormDataPart("assigned_pool", strPool);

                if (imageFile != null) {

                    //.................for normal  file...........................
                    String type1 = getMimType(imageFile.getPath());
                    Log.e("type1", "" + type1);
                    RequestBody requestBodyNormal = RequestBody.create(MediaType.parse(type1),
                            imageFile);
                    String file_path = imageFile.getAbsolutePath();
                    Log.e("file_path ", "" + file_path);
                    builder.addFormDataPart("type", type1)
                            .addFormDataPart("photo", file_path.substring(
                                    file_path.lastIndexOf("/") + 1), requestBodyNormal);


                }

                requestBody1 = builder.build();


                //  https://github.com/square/okhttp/blob/master/samples/guide/src/main/java/okhttp3/recipes/PostMultipart.java

                final okhttp3.Request request = new okhttp3.Request.Builder()
                        .url("" + UPLOAD_URL)
                        .post(requestBody1)
                        .build();


                try {
                    final okhttp3.Response response = client.newCall(request).execute();


                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (response.isSuccessful()) {
                                progressDialog.dismiss();
                                Log.e("success", "s " + response.body());
                                preparence.setPhoneNumber(contantNo);

                                finish();
                                startActivity(new Intent(
                                        ProfileActivity.this,
                                        VehicleRequisitionActivity.class));


//                                Toast.makeText(ProductionCreateImageActivity.this,
//                                        "success", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("not success ", "ns " + response);
                                progressDialog.dismiss();
                                Toast.makeText(ProfileActivity.this,
                                        "failed", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                } catch (IOException e) {
//                    Toast.makeText(ProductionCreateSuggestionActivity.this,
//                            "error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", "error s " + e.getMessage());
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }


        });
        t.start();

    }

    public String getMimType(String path) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(path);

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }


    public boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void checkPermission() {
        int PERMISSION_ALL = 1;
        try {
            if (!hasPermissions((this), PERMISSIONS)) {
                ActivityCompat.requestPermissions((this), PERMISSIONS, 12);
            }
        } catch (Exception e) {
        }
    }


    public void takePhoto() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_for_add_image);
        LinearLayout llTakePhoto, llGallery;
        TextView tvCancel;

        llTakePhoto = (LinearLayout) dialog.findViewById(R.id.llTakePhoto);
        llGallery = (LinearLayout) dialog.findViewById(R.id.llFromGallery);
        tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        llTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ProfileActivity.this,
                            new String[]{android.Manifest.permission.CAMERA},
                            3);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 2);
                }
                dialog.dismiss();

            }
        });

        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE);
                dialog.dismiss();

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && data != null) {
            setImage(data.getData());
        } else if (requestCode == 2) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Uri picUri = data.getData();//<- get Uri here from data intent
            if (picUri != null) {
                setImage(picUri);
            } else {
                Uri imageUri = getImageUri(this, bitmap);
                setImage(imageUri);
            }
        }
    }


    private void setImage(Uri imageUri) {
        String filePath1 = null;
        try {
            btnUpdate.setVisibility(View.VISIBLE);
            filePath1 = PathUtil.getPath(this, imageUri);
            imageFile = new File(filePath1);
            imageViewOfficer.setImageURI(imageUri);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("exception1", "ex " + e.getMessage());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 3) {
            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
            } else {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 2);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void loadData() {
        Call<LoginData> call = apiInterface.getAllLoginData(preparence.getPhoneNumber(),
                preparence.getPassword()
        );
        call.enqueue(loginResponse);
        progressDialog.show();
    }


    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        progressDialog.dismiss();
        //   AppController.getInstance().setLoginData(loginData);


        if (loginData != null) {
            this.loginData = loginData;
            this.poolsList = loginData.getPoolsList();
            setData();
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {

        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }


    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(
                ProfileActivity.this,
                VehicleRequisitionActivity.class));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                startActivity(new Intent(
                        ProfileActivity.this,
                        VehicleRequisitionActivity.class));
                break;
        }
        return true;
    }


}
