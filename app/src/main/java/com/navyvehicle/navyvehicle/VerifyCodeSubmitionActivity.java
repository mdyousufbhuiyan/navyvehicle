package com.navyvehicle.navyvehicle;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class VerifyCodeSubmitionActivity extends AppCompatActivity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final String SMS_API = "http://103.230.63.50/bulksms/api";
    public static final String CODE_SUBMIT_API = AppController.BASE_URL + "api/codeSubmit";
    //  EditText etOtp;
    Button btnSubmit;
    private String code, number, email;
    MySharedPreparence preparence;
    HashMap<String, String> params;
    ProgressDialog progressDialog;
    String retriveCode;
    String str[];
    ProgressBar progressBar;
    LinearLayout linearLayoutProgressBar;
    EditText etOne, etTwo, etThree, etFour;
    int counter = 1;
    boolean flag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code_submition);
        getSupportActionBar().setTitle("Verification Code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        // etOtp = (EditText) findViewById(R.id.etVerificationCode);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        linearLayoutProgressBar = (LinearLayout) findViewById(R.id.linearLayoutProgressBar);
        preparence = new MySharedPreparence(this);
        progressDialog = new ProgressDialog(this);
        code = getIntent().getStringExtra("code");
        number = getIntent().getStringExtra("number");
        email = getIntent().getStringExtra("email");


        params = new HashMap<String, String>();
        params.put("destination", "" + number);
        params.put("text", "Your Verification Code Is : " + code);
        params.put("requestId", String.valueOf(System.currentTimeMillis()));
        params.put("contentType", String.valueOf(1));
        params.put("authUser", "hi-techpark");
        params.put("authAccess", "hi-techpark");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveCode();
            }
        });


        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
        }

        new CallAPI("").execute();

    }

    private void retrieveCode() {
        if (!flag) {
            String one = etOne.getText().toString();
            String two = etTwo.getText().toString();
            String three = etThree.getText().toString();
            String four = etFour.getText().toString();
            if (!TextUtils.isEmpty(one) && !TextUtils.isEmpty(two) && !TextUtils.isEmpty(three) && !TextUtils.isEmpty(four)) {
                String str1 = one + two + three + four;
                if (code.equals(str1)) {
                    flag = true;
                    if (email != null) {
                        etOne.getText().clear();
                        etTwo.getText().clear();
                        etThree.getText().clear();
                        etFour.getText().clear();
                        upLoadCode();
                    } else {
//                        startActivity(new Intent(VerifyCodeSubmitionActivity.this, ChangePasswordActivity.class)
//                                .putExtra("number", number)
//                                .putExtra("code", code));
                        finish();

                    }
                } else {
                    // etOtp.setError("কোড টি ভুল");
                    Toast.makeText(VerifyCodeSubmitionActivity.this, "কোড টি ভুল", Toast.LENGTH_SHORT).show();
                }
            } else {

            }
        }
    }


    private void init() {
        etOne = (EditText) findViewById(R.id.etOne);
        etTwo = (EditText) findViewById(R.id.etTwo);
        etThree = (EditText) findViewById(R.id.etThree);
        etFour = (EditText) findViewById(R.id.etFour);
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                switch (counter) {

                    case 1:
                        etTwo.requestFocus();
                        counter++;
                        break;

                    case 2:
                        etThree.requestFocus();
                        counter++;
                        break;

                    case 3:
                        etFour.requestFocus();
                        counter++;
                        break;
                    case 4:

                        etFour.requestFocus();
                        counter++;
                        break;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        etOne.addTextChangedListener(textWatcher);
        etTwo.addTextChangedListener(textWatcher);
        etThree.addTextChangedListener(textWatcher);
        etFour.addTextChangedListener(textWatcher);

    }


    class CallAPI extends AsyncTask<Void, Void, Void> {
        String key;

        public CallAPI(String key) {
            this.key = key;

            //set context variables if required
        }

        @Override
        protected void onPreExecute() {
            if (!this.key.equals("wait")) {
                linearLayoutProgressBar.setVisibility(View.VISIBLE);
            }
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            if (this.key.equals("wait")) {

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else
                performPostCall(SMS_API, params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (key.equals("wait")) {
//                Toast.makeText(VerifyCodeSubmitionActivity.this, "" +
//                        "Code has been sent", Toast.LENGTH_SHORT).show();
                retrieveCode();
            }
        }
    }

    private void setVerificationCode() {
        String[] str = code.split("(?!^)");
        if (str.length == 4) {
            etOne.setText("" + str[0]);
            etTwo.setText("" + str[1]);
            etThree.setText("" + str[2]);
            etFour.setText("" + str[3]);
            new CallAPI("wait").execute();
            //retrieveCode();


        }
    }

    public String performPostCall(String requestURL,
                                  HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
            linearLayoutProgressBar.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
            linearLayoutProgressBar.setVisibility(View.GONE
            );
        }

        return response;
    }


    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public void upLoadCode() {

//        // Showing progress dialog at user registration time.
//        progressDialog.setMessage("Please Wait");
//        progressDialog.show();
//
//        //  Toast.makeText(this, "" + yearEn + "-" + String.valueOf(monthPosition) + "-" + String.valueOf(dayPosition), Toast.LENGTH_SHORT).show();
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("code", code);
//
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, CODE_SUBMIT_API, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            //   Toast.makeText(RegistrationActivity.this, "onResponse " + response, Toast.LENGTH_SHORT).show();
//
//                            String user_id = response.getString("user_id");
//                            preparence.setUserId(Integer.valueOf(user_id));
//                            preparence.setEmail(email);
//                            preparence.setLoginStatus(true);
//                            preparence.setSoundStatus(true);
//
//
//                            finish();
//
//
//                            // VolleyLog.v("Response:%n %s", response.toString(4));
//                            progressDialog.dismiss();
//                        } catch (Exception e) {
//                            Log.e("JSONException : ", "" + e.getMessage());
//                            // Toast.makeText(RegistrationActivity.this, "JSONException", Toast.LENGTH_SHORT).show();
//                            e.printStackTrace();
//                            progressDialog.dismiss();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //  Toast.makeText(RegistrationActivity.this, "VolleyError  " + error.getMessage(), Toast.LENGTH_SHORT).show();
//                // VolleyLog.e("Error: ", error.getMessage());
//                if (!isOnline(VerifyCodeSubmitionActivity.this)) {
//                    Toast.makeText(VerifyCodeSubmitionActivity.this, "দয়াকরে আপনার ইন্টারনেট সংযোগ চালু করুন ।", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                Log.e("Error :  ", "" + error);
//                Toast.makeText(VerifyCodeSubmitionActivity.this, "" + error, Toast.LENGTH_SHORT).show();
//                progressDialog.dismiss();
//            }
//        });

      //  MySingleton.getInstance(this).addToRequestQueue(req);
    }


    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                showAlertDialog("আপনার রেজিস্ট্রেশনটি এখনো সম্পূর্ণ হয়নি, আপনি কি রেজিস্ট্রেশন সম্পূর্ণ না করেই এই পেজ থেকে বের হতে চান?");
                break;
        }
        return true;
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //   Log.e("BroadcastReceiver", "  :  BroadcastReceiver");

            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.e("Tag", message);
                str = message.split(" ");
                if (str.length > 0)
                    retriveCode = str[str.length - 1];
                //   retrieveCode(message);

                if (code.equals(retriveCode)) {
                    //etOtp.setText("" + retrieveCode);
                    setVerificationCode();
                }

            }
        }
    };


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    //................show alert dialog...........................................................................................
    public void showAlertDialog(String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("" + message);
        alertDialogBuilder.setPositiveButton("হ্যাঁ",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("না", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
