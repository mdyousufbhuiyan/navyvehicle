package com.navyvehicle.navyvehicle.Callback;

import com.navyvehicle.navyvehicle.model.LoginData;

import retrofit2.Call;

public interface OnLoginValueListener {
    void OnLoginValueResponse(LoginData loginData);

    void OnLoginFailureResponse(Call<LoginData> call, Throwable t);
}
