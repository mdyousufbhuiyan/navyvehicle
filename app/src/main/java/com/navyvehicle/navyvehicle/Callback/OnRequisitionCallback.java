package com.navyvehicle.navyvehicle.Callback;

import com.navyvehicle.navyvehicle.model.LoginData;

public interface OnRequisitionCallback {

    public void customeShortClick(LoginData.Requisition requisition, int position);

    public void customeLongtClick(LoginData.Requisition requisition, int position);

}
