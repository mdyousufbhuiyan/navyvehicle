package com.navyvehicle.navyvehicle.middleware;

import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.model.LoginData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginResponse implements Callback<LoginData> {



    OnLoginValueListener onLoginValueListener;

    public LoginResponse(OnLoginValueListener onLoginValueListener) {
        this.onLoginValueListener = onLoginValueListener;
    }

    @Override
    public void onResponse(Call<LoginData> call, Response<LoginData> response) {
        LoginData loginData = response.body();
//        Log.e("Tag", "" + loginData.getMessage());
//        Log.e("Tag", "" + loginData.getPoolsList().size());

        onLoginValueListener.OnLoginValueResponse(loginData);
    }

    @Override
    public void onFailure(Call<LoginData> call, Throwable t) {
        onLoginValueListener.OnLoginFailureResponse(call, t);
    }
}
