package com.navyvehicle.navyvehicle;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {


    public static final String USER_DATA_URL = "http://navyvehicle.isslsolutions.com/api/getname";

    EditText etPhone;
    LinearLayout llMainLayout;
    MySharedPreparence preparence;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // getSupportActionBar().setTitle(".               Login");

        init();


        if (preparence.getLoginStaus()) {
            startActivity(new Intent(LoginActivity.this,
                    VehicleRequisitionActivity.class));
            finish();
        }

    }

    private void init() {
        progressDialog = new ProgressDialog(this);
        preparence = new MySharedPreparence(this);
        etPhone = (EditText) findViewById(R.id.etPhone);
        llMainLayout = findViewById(R.id.llMainLayout);


    }


    public void submitNavalId(final String id) {

        // Showing progress dialog at user registration time.
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("naval_id", String.valueOf(id));
        // Toast.makeText(QuizDesLabelActivity.this, "category_id : "+category_id , Toast.LENGTH_SHORT).show();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, USER_DATA_URL,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String name = response.getString("name");
                            String assigned_pool = response.getString("assigned_pool");

                            if (name != null && !name.equals("error")) {
                                preparence.setName(name);
                                preparence.setPool(assigned_pool);
                                Intent intent = new Intent(LoginActivity.this,
                                        PasswordActivity.class);
                                intent.putExtra("phone", id);
                                startActivity(intent);

                            } else {
                                etPhone.setError("Invalid");
                            }


                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            //Toast.makeText(QuizDesLabelActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(QuizDesLabelActivity.this, "VolleyError", Toast.LENGTH_SHORT).show();
                VolleyLog.e("Error: ", error.getMessage());
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(req);
    }


    public void btnLoginByPass(View view) {
        // Toast.makeText(this, "btnLoginByPass", Toast.LENGTH_SHORT).show();
        String phone = etPhone.getText().toString().trim();

        if (!TextUtils.isEmpty(phone)) {

            submitNavalId(phone);


//            llMainLayout.setVisibility(View.GONE);
//            PasswordFragment mFragment = null;
//            mFragment = new PasswordFragment();
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .add(R.id.container, mFragment).commit();
        } else {
            //  Toast.makeText(this, "Enter  NAVAL ID  or OTP", Toast.LENGTH_SHORT).show();
            if (TextUtils.isEmpty(phone)) {
                etPhone.setError("NAVAL ID field can't be empty");
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        llMainLayout.setVisibility(View.VISIBLE);
    }

    public void btnLoginByOtp(View view) {
    }

    public void tvForgetPass(View view) {
    }
}
