package com.navyvehicle.navyvehicle.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoginData {
    private String message;
    @SerializedName("users")
    private Users users;
    @SerializedName("designation")
    private Designation designation;

    @SerializedName("total_amenity")
    private String total_amenity;


    @SerializedName("paid_amenity")
    private String paid_amenity;

    @SerializedName("due_amenity")
    private String due_amenity;

    @SerializedName("ride_reason")
    private ArrayList<RideReason> rideReasonsList;
    @SerializedName("pool")
    private ArrayList<Pool> poolsList;
    @SerializedName("vehicle_category")
    private ArrayList<VehicleCategory> vehicleCategoryList;
    @SerializedName("requisitions")
    private ArrayList<Requisition> requisitionsList;
    private String zone;


    public String getTotal_amenity() {
        return total_amenity;
    }

    public void setTotal_amenity(String total_amenity) {
        this.total_amenity = total_amenity;
    }

    public String getPaid_amenity() {
        return paid_amenity;
    }

    public void setPaid_amenity(String paid_amenity) {
        this.paid_amenity = paid_amenity;
    }

    public String getDue_amenity() {
        return due_amenity;
    }

    public void setDue_amenity(String due_amenity) {
        this.due_amenity = due_amenity;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public ArrayList<RideReason> getRideReasonsList() {
        return rideReasonsList;
    }

    public void setRideReasonsList(ArrayList<RideReason> rideReasonsList) {
        this.rideReasonsList = rideReasonsList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public ArrayList<Pool> getPoolsList() {
        return poolsList;
    }

    public void setPoolsList(ArrayList<Pool> poolsList) {
        this.poolsList = poolsList;
    }

    public ArrayList<VehicleCategory> getVehicleCategoryList() {
        return vehicleCategoryList;
    }

    public void setVehicleCategoryList(ArrayList<VehicleCategory> vehicleCategoryList) {
        this.vehicleCategoryList = vehicleCategoryList;
    }

    public ArrayList<Requisition> getRequisitionsList() {
        return requisitionsList;
    }

    public void setRequisitionsList(ArrayList<Requisition> requisitionsList) {
        this.requisitionsList = requisitionsList;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public static class Users {
        private int id;
        private String first_name, last_name, naval_id, email, contact_no,
                username,assigned_pool, photo;

        public Users(int id, String first_name, String last_name, String naval_id,
                     String email, String contact_no, String username, String photo) {
            this.id = id;
            this.first_name = first_name;
            this.last_name = last_name;
            this.naval_id = naval_id;
            this.email = email;
            this.contact_no = contact_no;
            this.username = username;
            this.photo = photo;
        }


        public String getNaval_id() {
            return naval_id;
        }

        public void setNaval_id(String naval_id) {
            this.naval_id = naval_id;
        }


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAssigned_pool() {
            return assigned_pool;
        }

        public void setAssigned_pool(String assigned_pool) {
            this.assigned_pool = assigned_pool;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact_no() {
            return contact_no;
        }

        public void setContact_no(String contact_no) {
            this.contact_no = contact_no;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }


    public static class RideReason {
        private int id;
        private String name, description, status_id, created_by, updated_by, created_at,
                updated_at;

        public RideReason(int id, String name, String description, String status_id,
                          String created_by, String updated_by, String created_at,
                          String updated_at) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.status_id = status_id;
            this.created_by = created_by;
            this.updated_by = updated_by;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus_id() {
            return status_id;
        }

        public void setStatus_id(String status_id) {
            this.status_id = status_id;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }


    public static class Pool {
        private int id;
        private String name, description, status_id, created_by, updated_by, created_at,
                updated_at;


        public Pool(int id, String name, String description, String status_id,
                    String created_by, String updated_by, String created_at,
                    String updated_at) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.status_id = status_id;
            this.created_by = created_by;
            this.updated_by = updated_by;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus_id() {
            return status_id;
        }

        public void setStatus_id(String status_id) {
            this.status_id = status_id;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class VehicleCategory {
        private int id;
        private String name, description, status_id, created_by, updated_by, created_at,
                updated_at;


        public VehicleCategory(int id, String name, String description,
                               String status_id, String created_by, String updated_by,
                               String created_at, String updated_at) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.status_id = status_id;
            this.created_by = created_by;
            this.updated_by = updated_by;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus_id() {
            return status_id;
        }

        public void setStatus_id(String status_id) {
            this.status_id = status_id;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class Requisition {
        private String category, Car, vehicle, pool, driver_name, driver_contact, start_at,
                amenity, end_at, hour, kilomiter, status, start_time, end_time,
                driver_photo, amenity_status;
        private boolean isExpanded;
        private int amenity_check;

        public Requisition(String category, String car, String vehicle, String pool,
                           String driver_name, String driver_contact, String start_at,
                           String amenity, String end_at, String hour, String kilomiter,
                           String status, String start_time, String end_time, String driver_photo) {
            this.category = category;
            Car = car;
            this.vehicle = vehicle;
            this.pool = pool;
            this.driver_name = driver_name;
            this.driver_contact = driver_contact;
            this.start_at = start_at;
            this.amenity = amenity;
            this.end_at = end_at;
            this.hour = hour;
            this.kilomiter = kilomiter;
            this.status = status;
            this.start_time = start_time;
            this.end_time = end_time;
            this.driver_photo = driver_photo;
        }

        public int getAmenity_check() {
            return amenity_check;
        }

        public void setAmenity_check(int amenity_check) {
            this.amenity_check = amenity_check;
        }

        public String getAmenity_status() {
            return amenity_status;
        }

        public void setAmenity_status(String amenity_status) {
            this.amenity_status = amenity_status;
        }

        public String getDriver_photo() {
            return driver_photo;
        }

        public void setDriver_photo(String driver_photo) {
            this.driver_photo = driver_photo;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getAmenity() {
            return amenity;
        }

        public void setAmenity(String amenity) {
            this.amenity = amenity;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getCar() {
            return Car;
        }

        public void setCar(String car) {
            Car = car;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

        public String getPool() {
            return pool;
        }

        public void setPool(String pool) {
            this.pool = pool;
        }

        public String getDriver_name() {
            return driver_name;
        }

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        public String getDriver_contact() {
            return driver_contact;
        }

        public void setDriver_contact(String driver_contact) {
            this.driver_contact = driver_contact;
        }

        public String getStart_at() {
            return start_at;
        }

        public void setStart_at(String start_at) {
            this.start_at = start_at;
        }

        public String getEnd_at() {
            return end_at;
        }

        public void setEnd_at(String end_at) {
            this.end_at = end_at;
        }

        public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public String getKilomiter() {
            return kilomiter;
        }

        public void setKilomiter(String kilomiter) {
            this.kilomiter = kilomiter;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

//        @Override
//        public int describeContents() {
//            return 0;
//        }

//        public static final Creator<Requisition> CREATOR = new Creator<Requisition>() {
//            @Override
//            public Requisition createFromParcel(Parcel in) {
//                return new Requisition(in);
//            }
//
//            @Override
//            public Requisition[] newArray(int size) {
//                return new Requisition[size];
//            }
//        };
    }


    public static class Designation {
        String travel_hour, amenity_cost;

        public Designation(String travel_hour, String amenity_cost) {
            this.travel_hour = travel_hour;
            this.amenity_cost = amenity_cost;
        }

        public String getTravel_hour() {
            return travel_hour;
        }

        public void setTravel_hour(String travel_hour) {
            this.travel_hour = travel_hour;
        }

        public String getAmenity_cost() {
            return amenity_cost;
        }

        public void setAmenity_cost(String amenity_cost) {
            this.amenity_cost = amenity_cost;
        }
    }


}
