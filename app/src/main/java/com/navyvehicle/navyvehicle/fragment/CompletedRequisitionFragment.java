package com.navyvehicle.navyvehicle.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.Callback.OnRequisitionCallback;
import com.navyvehicle.navyvehicle.MySharedPreparence;
import com.navyvehicle.navyvehicle.R;
import com.navyvehicle.navyvehicle.adapter.MyTripAdapter;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedRequisitionFragment extends Fragment implements OnLoginValueListener, OnRequisitionCallback {
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private MySharedPreparence preparence;
    private RecyclerView recyclerView;
    private ArrayList<LoginData.Requisition> requisitionsList, requisitionsFilteredList;
    private LoginResponse loginResponse;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm a");

    private View view;


    private LoginData loginData;


    public CompletedRequisitionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_completed_requisition, container, false);

        init();
        return view;
    }


    private void init() {
        requisitionsList = new ArrayList<>();
        requisitionsFilteredList = new ArrayList<>();
        preparence = new MySharedPreparence(getActivity());


        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait");
        loginResponse = new LoginResponse((OnLoginValueListener) this);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        if (loginData == null) {
            loadData();
        } else {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView();
        }

    }

    private void prepareRecycleView() {

        sortByDate();
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        MyTripAdapter myTripAdapter = new MyTripAdapter(
                requisitionsFilteredList, getActivity(), loginData,
                this);

        recyclerView.setAdapter(myTripAdapter);
    }

    private void filterList() {
        LoginData.Requisition requisition = null;
        boolean flag = true;
        for (int i = 0; i < requisitionsList.size(); i++) {
            if (requisitionsList.get(i).getStatus() != null) {
                if (requisitionsList.get(i).getStatus().equals("Rejected")) {
                    requisitionsFilteredList.add(requisitionsList.get(i));

                } else if (requisitionsList.get(i).getStatus().equals("Completed")) {
                    requisitionsFilteredList.add(requisitionsList.get(i));

                }
            }
        }
    }


    private void sortByDate() {
        Collections.sort(requisitionsList, new Comparator<LoginData.Requisition>() {
            @Override
            public int compare(LoginData.Requisition o1, LoginData.Requisition o2) {
                int compareResult = 0;
                try {
                    Date arg0Date = format.parse(o1.getStart_time());
                    Date arg1Date = format.parse(o2.getStart_time());
                    compareResult = arg1Date.compareTo(arg0Date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    compareResult = o2.getStart_time().compareTo(o1.getStart_time());
                }
                return compareResult;
            }
        });
        filterList();
    }


    @Override
    public void customeShortClick(LoginData.Requisition requisition, int position) {

    }

    @Override
    public void customeLongtClick(LoginData.Requisition requisition, int position) {

    }

    private void loadData() {
        Call<LoginData> call = apiInterface.getAllLoginData(preparence.getPhoneNumber(),
                preparence.getPassword()
        );
        call.enqueue(loginResponse);
        progressDialog.show();
    }


    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        progressDialog.dismiss();
        //   AppController.getInstance().setLoginData(loginData);
        this.loginData = loginData;
        if (loginData != null) {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView();
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {

        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }


}
