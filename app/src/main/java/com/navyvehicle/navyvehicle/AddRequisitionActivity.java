package com.navyvehicle.navyvehicle;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;

public class AddRequisitionActivity extends AppCompatActivity implements OnLoginValueListener {
    private static final String SUBMIT_URL = "http://navyvehicle.isslsolutions.com/api/vehicle_request";
    int mYear, mMonth, mDay, mHour, mMinute;
    private String strNumber, strStartDate, strStartTime, strStartTimeWithoutFormate, strEndDate,
            strEndTime, strEndTimeWithoutFormate, strAmenity, strCategory, strPool, strResons, strDestination, strResonsDes;
    private Spinner spResons, spAssignPool, spVehicleCategory;
    private TextView tvStartTime, tvEndTime, tvEstimatedTime, tvEstimatedAmenity;
    private LinearLayout llEstimatedAmenity;
    private EditText etNumber, etDestination, etReason;

    private ArrayList<LoginData.Pool> poolsList;
    private ArrayList<LoginData.VehicleCategory> vehicleCategoryList;
    private ArrayList<LoginData.RideReason> rideReasonsList;
    private ArrayList<String> vehicleNameList, poolNameList, reasonsList;
    private ProgressDialog progressDialog;
    private APIInterface apiInterface;
    private LoginResponse loginResponse;
    private MySharedPreparence preparence;
    private LoginData loginData;
    private LoginData.Designation designation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_requisition);
        getSupportActionBar().setTitle("Vehicle Requisition");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        loadData();
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        loginResponse = new LoginResponse(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");


        llEstimatedAmenity = findViewById(R.id.llEstimatedAmenity);
        tvEstimatedTime = findViewById(R.id.tvEstimatedTime);
        tvEstimatedAmenity = findViewById(R.id.tvEstimatedAmenity);
        tvStartTime = findViewById(R.id.tvStartTime);
        tvEndTime = findViewById(R.id.tvEndTime);
        etNumber = findViewById(R.id.etNumber);
        etDestination = findViewById(R.id.etDestination);
        etReason = findViewById(R.id.etReason);
        spAssignPool = findViewById(R.id.spAssignPool);
        spResons = findViewById(R.id.spResons);
        spVehicleCategory = findViewById(R.id.spVehicleCategory);


        reasonsList = new ArrayList<>();
        poolNameList = new ArrayList<>();
        vehicleNameList = new ArrayList<>();
        vehicleCategoryList = new ArrayList<>();
        rideReasonsList = new ArrayList<>();
        poolsList = new ArrayList<>();


    }

    private void setSpinnerData() {
        vehicleNameList.add(0, "Select Vehicle Category");
        poolNameList.add(0, "Select Pool");
        reasonsList.add(0, "Select Reason");

        if (loginData != null) {
            vehicleCategoryList = loginData.getVehicleCategoryList();
            poolsList = loginData.getPoolsList();
            rideReasonsList = loginData.getRideReasonsList();
        }
        if (poolsList != null && vehicleCategoryList != null)
            Log.e("TAG", "vehicleCategoryList: " + vehicleCategoryList.size() + " poolsList " + poolsList.size());


        if (poolsList != null)
            setPoolNameList();
        if (vehicleCategoryList != null)
            setVehicleNameList();

        if (rideReasonsList != null) {
            setResonsList();
        }

    }


    private void setResonsList() {
        for (LoginData.RideReason rr : rideReasonsList) {
            reasonsList.add(rr.getName());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1
                , android.R.id.text1, reasonsList);
        spResons.setAdapter(arrayAdapter);
        spResons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {

                    strResons = String.valueOf(rideReasonsList.get(position - 1).getId());
                    // Toast.makeText(AddRequisitionActivity.this, "strResons : "+strResons, Toast.LENGTH_SHORT).show();
                    if (rideReasonsList.get(position - 1).getName().equals("Others")) {
                        etReason.setVisibility(View.VISIBLE);
                    } else {
                        etReason.setVisibility(View.GONE);
                    }
                } else {
                    strResons = null;
                    etReason.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setVehicleNameList() {
        for (LoginData.VehicleCategory vc : vehicleCategoryList) {
            vehicleNameList.add(vc.getName());
        }


        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1
                , android.R.id.text1, vehicleNameList);
        spVehicleCategory.setAdapter(arrayAdapter);
        spVehicleCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    strCategory = String.valueOf(
                            vehicleCategoryList.get(position - 1).getId());

                else
                    strCategory = null;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setPoolNameList() {
        for (LoginData.Pool pn : poolsList) {
            poolNameList.add(pn.getName());
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1
                , android.R.id.text1, poolNameList);
        spAssignPool.setAdapter(arrayAdapter);
        spAssignPool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    strPool = String.valueOf(poolsList.get(position - 1).getId());
                else
                    strPool = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void btnSubmit(View view) {
        strNumber = etNumber.getText().toString();
        strResonsDes = etReason.getText().toString();
        strDestination = etDestination.getText().toString();

        if (strStartDate != null && strEndDate != null
                && !TextUtils.isEmpty(strAmenity)
                && strCategory != null &&
                !strCategory.equals("Select Vehicle Category") &&
                !TextUtils.isEmpty(strNumber) &&
                !TextUtils.isEmpty(strDestination)
        ) {
            submitData();
        } else {

//            if (strPool == null || strPool.equals("Select Pool")) {
//                TextView errorText = (TextView) spAssignPool.getSelectedView();
//                errorText.setError("");
//                errorText.setTextColor(Color.RED);//just to highlight that this is an error
//                // errorText.setText("অনুগ্রহ করে দিন নির্বাচন করুন");
//                // errorText.setText("দিন");
//            }
            if (strCategory == null || strCategory.equals("Select Category")) {
                TextView errorText = (TextView) spVehicleCategory.getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);//just to highlight that this is an error

            }
            if (TextUtils.isEmpty(strNumber)) {
                etNumber.setError("Phone field can't be empty");//just to highlight that this is an error

            }
            if (TextUtils.isEmpty(strDestination)) {
                etNumber.setError("Destination field can't be empty");//just to highlight that this is an error

            }
            Toast.makeText(this, "Please Give Required Information"
                    , Toast.LENGTH_SHORT).show();
        }
    }


    private void datePicker(final String command) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                        //*************Call Time Picker Here ********************

                        if (command.equals("start")) {
                            strStartDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        } else {
                            strEndDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }


                        tiemPicker(command);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    private void tiemPicker(final String command) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);


        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        int s = minute / 2;


                        String format, strHour = "", strWFHour = "", strMinute = "", strWFMinute = "";
                        if (mHour < 10) {
                            strWFHour = "0" + mHour;
                        } else {
                            strWFHour = "" + mHour;

                        }
                        if (mMinute < 10) {
                            strWFMinute = "0" + mMinute;
                        } else {
                            strWFMinute = "" + mMinute;
                        }

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        if (hourOfDay < 10) {
                            strHour = "0" + hourOfDay;
                        } else {
                            strHour = "" + hourOfDay;
                        }

                        if (minute < 10) {
                            strMinute = "0" + minute;

                        } else {
                            strMinute = "" + minute;
                        }

                        if (command.equals("start")) {
                            //   strStartTime = mHour + ":" + minute + ":" + s;
                            strStartTime = strHour + ":" + strMinute + " " + format;
                            strStartTimeWithoutFormate = strWFHour + ":" + strWFMinute;
                            tvStartTime.setText("" + strStartDate + " " + strStartTimeWithoutFormate);


                        } else {
                            //   strEndTime = mHour + ":" + minute + ":" + s;
                            strEndTime = strHour + ":" + strMinute + " " + format;
                            strEndTimeWithoutFormate = strWFHour + ":" + strWFMinute;
                            tvEndTime.setText("" + strEndDate + " " + strEndTimeWithoutFormate);

                        }

                        setEstimatedTimeAndAmenity();
                        //   tvPublishDate.setText(strStartDate + " " + strStartTime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    private void setEstimatedTimeAndAmenity() {
        if (strStartTime != null && strEndTime != null && strAmenity != null && strAmenity.equals("y")) {


            int totalHour = getEstimatedHour(strStartDate
                    , strStartTime, strEndDate, strEndTime);
            int amenity = getEstimatedAmenity(totalHour);

            tvEstimatedAmenity.setText(amenity + " BDT");
            if (totalHour > 1) {
                tvEstimatedTime.setText(totalHour + " Hours");

            } else {
                tvEstimatedTime.setText(totalHour + " Hour");
            }

            llEstimatedAmenity.setVisibility(
                    View.VISIBLE);
        } else {
            llEstimatedAmenity.setVisibility(View.GONE);
        }
    }


    public void tvStartTime(View view) {
        datePicker("start");
    }

    public void tvEndTime(View view) {
        datePicker("end");
    }

    public void rbWithoutAmenity(View view) {
        strAmenity = "n";
        setEstimatedTimeAndAmenity();
    }

    public void rbWithAmenity(View view) {
        strAmenity = "y";
        setEstimatedTimeAndAmenity();
    }


    public void submitData() {
        // Showing progress dialog at user registration time.
        progressDialog.setMessage("Please Wait");
        progressDialog.show();

        //Log.e("counter_time", " " + totalTime);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("zone", loginData.getZone());
        params.put("requisition_reason", strResons);
        params.put("amenity", strAmenity);
        params.put("contact_no", strNumber);

        params.put("officers_id", String.valueOf(
                loginData.getUsers().getId()));
        params.put("pool_id", preparence.getPool());
        params.put("destination", strDestination);
        params.put("reason_description", strResonsDes);
        params.put("category", strCategory);
        params.put("start_time", strStartDate + " " + strStartTimeWithoutFormate);
        params.put("end_time", strEndDate + " " + strEndTimeWithoutFormate);

        final JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, SUBMIT_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            //  Toast.makeText(MiniCertificateActivity.this, "" + response, Toast.LENGTH_SHORT).show();

                            String message = response.getString("message");
                            if (message != null && message.equals("success")) {
                                Toast.makeText(AddRequisitionActivity.this,
                                        "Success", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(AddRequisitionActivity.this,
                                        VehicleRequisitionActivity.class);

                                startActivity(intent);
                            } else {
                                Toast.makeText(AddRequisitionActivity.this,
                                        "Failed", Toast.LENGTH_SHORT).show();
                            }

                            Log.e("response", "response : " + response);
//                            Toast.makeText(AddRequisitionActivity.this,
//                                    "response", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            VolleyLog.v("Response:%n %s", response.toString(4));
                        } catch (JSONException e) {
                            // Toast.makeText(MiniCertificateActivity.this, "JSONException", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(MiniCertificateActivity.this, "VolleyError", Toast.LENGTH_SHORT).show();
                VolleyLog.e("Error: ", error.getMessage());
                Log.e("Error :  ", "" + error.getMessage());
                progressDialog.dismiss();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(req);
    }

    private void loadData() {
        Call<LoginData> call = apiInterface.getAllLoginData(preparence.getPhoneNumber(),
                preparence.getPassword()
        );
        call.enqueue(loginResponse);
        progressDialog.show();
    }


    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        progressDialog.dismiss();
        //   AppController.getInstance().setLoginData(loginData);
        this.loginData = loginData;
        if (loginData != null) {
            designation = loginData.getDesignation();
            setSpinnerData();
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {

        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }


    private int getEstimatedHour(String date1, String time1, String date2, String time2) {

        try {
//            String date1 = "07/15/2016";
//            String time1 = "11:00 AM";
//            String date2 = "07/17/2016";
//            String time2 = "12:15 AM";

            //  String format = "MM/dd/yyyy hh:mm";
            String format = "yyyy-MM-dd hh:mm a";

            //SimpleDateFormat sdf = new SimpleDateFormat(format);

            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);

            Date dateObj1 = sdf.parse(date1 + " " + time1);
            Date dateObj2 = sdf.parse(date2 + " " + time2);
            System.out.println(dateObj1);
            System.out.println(dateObj2 + "\n");

            DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");

            // getTime() returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this Date object
            long diff = dateObj2.getTime() - dateObj1.getTime();

//            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
//            System.out.println("difference between days: " + diffDays);

            int diffhours = (int) (diff / (60 * 60 * 1000));
            double remainHour = (diff % (60 * 60 * 1000));

            if (remainHour > 0) {
                diffhours += 1;
            }

            if (designation != null) {
                int freeHour = Integer.valueOf(designation.getTravel_hour());
                int estimatedOurForAmenity = diffhours - freeHour;
                if (estimatedOurForAmenity > 0) {
                    return estimatedOurForAmenity;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Date", "" + e.getMessage());
        }

        return 0;
    }


    private int getEstimatedAmenity(int estimatedOur) {
        if (designation != null) {
            int amenity = Integer.valueOf(designation.getAmenity_cost()) * estimatedOur;
            return amenity;
        }

        return 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                Intent intent = new Intent(AddRequisitionActivity.this,
                        VehicleRequisitionActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}
