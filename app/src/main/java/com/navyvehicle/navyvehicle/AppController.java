package com.navyvehicle.navyvehicle;

import android.app.Application;

import com.navyvehicle.navyvehicle.model.LoginData;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by issl on 2/14/2018.
 */

public class AppController extends Application {
    public static final String BASE_URL = "http://navyvehicle.isslsolutions.com";
    private static AppController mInstance;

    private LoginData loginData;


    @Override
    public void onCreate() {
        mInstance = this;
        super.onCreate();
        Picasso.Builder pBuilder = new Picasso.Builder(this);
        pBuilder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = pBuilder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }
}
