package com.navyvehicle.navyvehicle;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;

import retrofit2.Call;

public class PasswordActivity extends AppCompatActivity implements OnLoginValueListener {
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private LoginResponse loginResponse;
    private EditText etPassword, etPhone, etName;
    private Button btnSubmit;
    private String phoneNumber;
    private MySharedPreparence preparence;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        phoneNumber = getIntent().getStringExtra("phone");

        init();
    }

    private void init() {
        preparence = new MySharedPreparence(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        loginResponse = new LoginResponse(this);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPhone = (EditText) findViewById(R.id.etPhone);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        etName = findViewById(R.id.etName);
        etPhone.setText(phoneNumber);
        etName.setText(preparence.getName());

    }


    public void btnSubmit(View view) {
        loginByPass();

    }

    public void loginByPass() {
        // Toast.makeText(this, "btnLoginByPass", Toast.LENGTH_SHORT).show();
        password = etPassword.getText().toString();
        if (!TextUtils.isEmpty(password)) {

            Call<LoginData> call = apiInterface.getAllLoginData(phoneNumber,
                    password
            );
            call.enqueue(loginResponse);
            progressDialog.show();

        } else {
            if (TextUtils.isEmpty(password)) {
                etPassword.setError("Password field can't be empty");
            }
        }

    }

    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        Log.e("message", "" + loginData.getMessage());
        Log.e("message", "" + loginData.getZone());
        //   AppController.getInstance().setLoginData(loginData);
        if (loginData.getMessage() != null && loginData.getMessage().equals("success")) {
            subscribeForNotification(loginData.getUsers().getContact_no());
        } else {
            Toast.makeText(this, "Phone number or password is invalid", Toast.LENGTH_SHORT).show();

            progressDialog.dismiss();
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {

        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }

    private void subscribeForNotification(String subscribeId) {
        FirebaseMessaging.getInstance().subscribeToTopic(subscribeId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "default";
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            preparence.setLoginStatus(true);
                            preparence.setPhoneNumber(phoneNumber);
                            preparence.setPassword(password);
                            startActivity(new Intent(PasswordActivity.this,
                                    VehicleRequisitionActivity.class));
                            finish();
                        } else {
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(PasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
