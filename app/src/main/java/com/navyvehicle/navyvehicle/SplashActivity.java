package com.navyvehicle.navyvehicle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    Thread thread = null;
    private boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    // thread.stop();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (flag)
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));

            }
        });
        thread.start();

    }

    public void llParent(View view) {
        if (thread.isAlive()) {
            flag = false;
        }
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
    }
}
