package com.navyvehicle.navyvehicle;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.navyvehicle.navyvehicle.Callback.OnLoginValueListener;
import com.navyvehicle.navyvehicle.Callback.OnRequisitionCallback;
import com.navyvehicle.navyvehicle.adapter.HistoryAdapter;
import com.navyvehicle.navyvehicle.middleware.LoginResponse;
import com.navyvehicle.navyvehicle.model.LoginData;
import com.navyvehicle.navyvehicle.retrofit.APIClient;
import com.navyvehicle.navyvehicle.retrofit.APIInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

public class AmenityHistoryActivity extends AppCompatActivity implements OnLoginValueListener,
        OnRequisitionCallback {
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private MySharedPreparence preparence;
    private RecyclerView recyclerView;
    private ArrayList<LoginData.Requisition> requisitionsList, requisitionsFilteredList;
    private LoginResponse loginResponse;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm a");
    private TextView tvEnd, tvStarts;
    private View view;
    private String strStartDate, strEndDate;
    private int mYear, mMonth, mDay;
    private LoginData loginData;
    private HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amenity_history);
        getSupportActionBar().setTitle("Amenity Reports");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }


    private void init() {
        requisitionsList = new ArrayList<>();
        requisitionsFilteredList = new ArrayList<>();
        preparence = new MySharedPreparence(this);


        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        loginResponse = new LoginResponse((OnLoginValueListener) this);
        tvStarts = (TextView) findViewById(R.id.tvStarts);
        tvEnd = (TextView) findViewById(R.id.tvEnd);

        tvStarts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker("start");
            }
        });
        tvEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker("end");
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        if (loginData == null) {
            loadData();
        } else {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView(requisitionsList);
        }

    }

    private void prepareRecycleView(ArrayList<LoginData.Requisition> list) {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        historyAdapter = new HistoryAdapter(
                list, loginData, this, this,
                "history");
        recyclerView.setAdapter(historyAdapter);
    }

    private void filterList() {
        LoginData.Requisition requisition = null;
        boolean flag = true;
        for (int i = 0; i < requisitionsList.size(); i++) {
            if (requisitionsList.get(i).getStatus() != null) {
                if (requisitionsList.get(i).getStatus().equals("Rejected")) {
                    requisitionsFilteredList.add(requisitionsList.get(i));

                } else if (requisitionsList.get(i).getStatus().equals("Completed")) {
                    requisitionsFilteredList.add(requisitionsList.get(i));

                }
            }
        }
    }


    private void sortByDate(ArrayList<LoginData.Requisition> list) {
        Collections.sort(list, new Comparator<LoginData.Requisition>() {
            @Override
            public int compare(LoginData.Requisition o1, LoginData.Requisition o2) {
                int compareResult = 0;
                try {
                    Date arg0Date = format.parse(o1.getStart_time());
                    Date arg1Date = format.parse(o2.getStart_time());
                    compareResult = arg1Date.compareTo(arg0Date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    compareResult = o2.getStart_time().compareTo(o1.getStart_time());
                }
                return compareResult;
            }
        });
        prepareRecycleView(list);
    }

    private void datePicker(final String command) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        //*************Call Time Picker Here ********************

                        if (command.equals("start")) {
                            strStartDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            tvStarts.setText(strStartDate);

                            if (strEndDate != null)
                                filterByDate(strStartDate, strEndDate);
                        } else {
                            strEndDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            tvEnd.setText(strEndDate);
                            if (strStartDate != null)
                                filterByDate(strStartDate, strEndDate);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    private void filterByDate(String date1, String date2) {
        ArrayList<LoginData.Requisition> tempRequisitionsList = new ArrayList<>();
        try {
            String format = "yyyy-MM-dd";
            String format1 = "yyyy-MM-dd hh:mm:ss";

            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            SimpleDateFormat sdf1 = new SimpleDateFormat(format1, Locale.ENGLISH);

            Date dateObj1 = sdf.parse(date1);
            Date dateObj2 = sdf.parse(date2);

            if (dateObj1.compareTo(dateObj2) > 0) {
                Date t = dateObj1;
                dateObj1 = dateObj2;
                dateObj2 = t;
            }

            int totalAmenity = 0, paidAmenity = 0, dueAmenity = 0;
            for (LoginData.Requisition r : requisitionsList) {
                Date newDateObj1 = sdf1.parse(r.getStart_time());
                Date newDateObj2 = sdf1.parse(r.getEnd_time());
                if ((newDateObj1.compareTo(dateObj1) > 0 || newDateObj1.compareTo(dateObj1) == 0) &&
                        (newDateObj2.compareTo(dateObj2) < 0 || newDateObj2.compareTo(dateObj2) == 0)) {


                    tempRequisitionsList.add(r);
                    int currentAmenity = 0;
                    if (r.getAmenity() != null) {
                        currentAmenity = Integer.valueOf(r.getAmenity());
                    }

                    if (r.getAmenity_check() != 0) {
                        totalAmenity += currentAmenity;

                        if (r.getAmenity_status() != null && Integer.valueOf(
                                r.getAmenity_status()) == 0) {
                            dueAmenity += currentAmenity;
                        } else {
                            paidAmenity += currentAmenity;
                        }

                    }
                }

            }

            loginData.setPaid_amenity(String.valueOf(paidAmenity));
            loginData.setDue_amenity(String.valueOf(dueAmenity));
            loginData.setTotal_amenity(String.valueOf(totalAmenity));
            sortByDate(tempRequisitionsList);
            // historyAdapter.filterList(tempRequisitionsList);


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Date", "" + e.getMessage());
        }
    }


    @Override
    public void customeShortClick(LoginData.Requisition requisition, int position) {

    }

    @Override
    public void customeLongtClick(LoginData.Requisition requisition, int position) {

    }

    private void loadData() {
        Call<LoginData> call = apiInterface.getAllLoginData(preparence.getPhoneNumber(),
                preparence.getPassword()
        );
        call.enqueue(loginResponse);
        progressDialog.show();
    }


    @Override
    public void OnLoginValueResponse(LoginData loginData) {
        progressDialog.dismiss();
        //   AppController.getInstance().setLoginData(loginData);
        this.loginData = loginData;
        if (loginData != null) {
            requisitionsList = loginData.getRequisitionsList();
            prepareRecycleView(requisitionsList);
        }

    }

    @Override
    public void OnLoginFailureResponse(Call<LoginData> call, Throwable t) {

        progressDialog.dismiss();
        Log.e("message", "" + t.getMessage());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }
}
